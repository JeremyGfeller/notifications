import Vue from 'vue'
import App from './App.vue'
import VueFire from 'vuefire'
import BootstrapVue from 'bootstrap-vue'
import router from './router'
import firebase from 'firebase'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue, VueFire)

Vue.config.productionTip = false

var config = {
  apiKey: "AIzaSyBJsQ0a9PoCh9vAHrIqBZAgSil4-chGEf8",
  authDomain: "iomedia-notif.firebaseapp.com",
  databaseURL: "https://iomedia-notif.firebaseio.com",
  projectId: "iomedia-notif",
  storageBucket: "iomedia-notif.appspot.com",
  messagingSenderId: "146593890149",
  appId: "1:146593890149:web:f44f3b7552cf7c97"
};
firebase.initializeApp(config);
export var messaging = firebase.messaging();
export var database = firebase.database();
export const key = 'key=AAAAIiGtP2U:APA91bE6fFYUxJBKO7mvpj2lJaEbtWP12SrCZAmruhm8UimuDg40kENlIdjY0ytrOXfzWypUa6wDm6mn04c2NC2AOt71q-mvySKljERA5XJtvjMXgc_C1xBCDgejjx2BCxomNWt-UX2-'

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')

messaging.onMessage((payload) => {
  const notificationTitle = payload.notification.title;
  const notificationOptions = {
      body: payload.notification.body,
      icon: payload.notification.icon
  };
  navigator.serviceWorker.ready.then(registration => {
    registration.showNotification(notificationTitle, notificationOptions);
  });
});